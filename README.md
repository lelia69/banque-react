Au cours de ma formation 'Developpeur Web/ developpeur Mobile' , nous devions, en guise de cinquième projet, créer une application de gestion de budget. 


L'application doit permettre de gérer son budget : Indiquer les différentes dépenses (et éventuellement entrées) d'argent avec le montant, la catégorie de dépense/entrée et un petit titre si on souhaite le préciser.

Le lien de ma maquette fonctionnelle se trouve ci dessous :

https://whimsical.com/banque-SjbyGZaX9uMMU2ufW1CM9o
