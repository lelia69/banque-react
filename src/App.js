import './App.css';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Home from './Pages/Home';
import Board from './Pages/Board';


function App() {
  return (
    <BrowserRouter>

      <div className="App">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/board/">
           <Board/>
          </Route>
          
        </Switch>
      </div>

    </BrowserRouter>

  );
}

export default App;
