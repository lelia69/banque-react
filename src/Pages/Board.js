import './Board.css';
import NavBar from '../Components/Navabar';
import { useEffect, useState } from 'react';
import OperationsService from '../shared/operations-service';
import Modal from '../Components/Modal';
import SoldeService from '../shared/solde-service';
import SoldeForm from '../Components/SoldeForm';
import Operations from '../Components/Operations2';




export default function Board() {


    const [ope, setOpe] = useState([]);
    const [loading, setLoading] = useState(true);
    const [total, setTotal] = useState(0);
    const [budget, setBudget] = useState(0);

    const [solde, setSolde] = useState([])








    useEffect(() => {

        async function fetchSolde() {
            const reponse = await new SoldeService().fetchAllSolde();
            setSolde(reponse.data)
            console.log(reponse);
            setBudget(reponse.data[0].total)


        }



        async function fetchOpe() {
            const data = await new OperationsService().fetchAllOpe();
            if (!data) {
                return null
            }
            setOpe(data.data);
            let tot = 0;
            for (const item of data.data) {
                tot -= item.total
            }
            setTotal(budget + tot);




        } fetchOpe();
        fetchSolde();
        setLoading(false);





    }, [budget])

    async function deleteOperations(id) {
        await new OperationsService().deleteOpe(id);
        setOpe(
            ope.filter(item => item.id !== id)
        );


    }

    /*async function newSolde(){
        const response= await new SoldeService().updateSolde(solde);
        setSolde(response.data)
 
 
     }*/

     

    return (

        <div className="contain" >
            <nav>
                <NavBar />
            </nav>
            <section className="background"
                style={{
                    '--color-1': 'deepskyblue',
                    '--color-2': 'navy',
                    background: `
              linear-gradient(
                170deg,
                var(--color-1),
                var(--color-2) 80%
              )
            `,
                }}>


                <Modal />





                <section className="soldeBoard">
                    <article className="diplaySolde">
                        <p> Solde actuel : {total} €</p>
                    </article>
                    <SoldeForm />
                </section>

                {loading !== true ? ope.map((value, index) => {
                    return <Operations key={value.operaid} operations={value} onDelete={deleteOperations} />
                }) : <p>chargement en cours..</p>}
            </section>



        </div>

    )
}