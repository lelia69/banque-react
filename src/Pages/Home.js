import { useEffect, useState } from 'react';
import Solde from '../Components/Solde';
import './Home.css';
import ReactTooltip from 'react-tooltip'
import SoldeService from '../shared/solde-service';
import { Link } from 'react-router-dom';







export default function Home() {
    const [solde, setSolde] = useState([])
    const [loading, setLoading] = useState(true);

    async function fetchSolde() {
        const reponse = await new SoldeService().fetchAllSolde();
        setSolde(reponse.data)
        console.log(reponse);
        setLoading(false);
    }


    useEffect(() => {
        fetchSolde();
    }, []);

    return (

        <div className="container">

            <section className="home"
                style={{
                    '--color-1': 'deepskyblue',
                    '--color-2': 'navy',
                    background: `
                  linear-gradient(
                    170deg,
                    var(--color-1),
                    var(--color-2) 80%
                  )
                `,
                }}>
                <h1>Lili  <span className="spanou">Count</span></h1>
                <article className="article">
                    <h4>Application de dépenses impulsives</h4>
                    <Link to={'/board/'}><button className="btn-compte">Accéder à mes Comptes</button></Link> <br /> <br />
                    <button className="btn-solde" data-tip data-for="tool">Voir mon Solde</button>

                    <ReactTooltip id="tool" place="bottom" effect="solid" border="true" borderColor="rgb(238, 170, 44)" arrowColor="rgb(238, 170, 44)">
                        {loading ? null : <Solde solde={solde} />}
                    </ReactTooltip>



                </article>


            </section>
        </div>
    )
}