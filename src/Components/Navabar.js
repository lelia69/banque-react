
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { Link } from 'react-router-dom';
import grey from '@material-ui/core/colors/grey';
import indigo from '@material-ui/core/colors/indigo';
import amber from '@material-ui/core/colors/amber';



const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
   
  },
  menuButton: {
    marginRight: theme.spacing(2),
    
  },
  title: {
    flexGrow: 1,
    color : indigo[900],
    letterSpacing: 8,
    fontFamily : 'Contrail One', 
    textAlign: "center",
    fontSize : 25
    
  },

  nav :{
    background: grey[50],
    height: 80,
    
 
  },

  navy :{
    borderBottom : 'solid', 
    color : amber[600],
  
    
  }
}));

export default function NavBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static"  className={classes.nav}>
        <Toolbar className={classes.navy}>
        <Link to={"/"}> <ArrowBackIosIcon edge="start" className={classes.menuButton} color="white" aria-label="menu">
            <MenuIcon />
          </ArrowBackIosIcon></Link>
          <Typography variant="h6" className={classes.title}>
            Board
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}