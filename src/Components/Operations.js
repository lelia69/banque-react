
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import 'moment/locale/fr';
 import './FormOperations.css'


const useStyles = makeStyles({
  root: {
    width: 350,
    marginTop: 10,
    height: 150,
    
    
  },
  
  title: {
    fontSize: 10,
  },
  pos: {
    marginBottom: 12,
  },
  btn: {
    marginLeft : 10
    
  }
});

export default function OperationsCard({operations, onDelete}) {

  const classes = useStyles();


  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <CardActions>
        <Button className={classes.btn} size="small" color="secondary" onClick={() => onDelete(operations.operaid)}>Delete</Button>
      </CardActions>
        <Typography className={classes.title} variant="h5" component="h2" color="textSecondary" gutterBottom>
          {operations.title}
        </Typography>
        <Typography  >
          {operations.total}€
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {moment(operations.date).fromNow()}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          {operations.catid}
        </Typography>
        
      </CardContent>
      
    </Card>
  );
}