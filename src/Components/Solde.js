export default function Solde({solde}){
    return(
        <div>
           { solde.map((value, index)=>{
               return <p key={value.soldeid}>Mon solde entre le {new Intl.DateTimeFormat('fr-FR').format(new Date(value.date_start))} et le {new Intl.DateTimeFormat('fr-FR').format(new Date(value.date_end))}  est de {value.total} € </p>
           })}
        </div>
    )
}