import { useState } from 'react';
import './SoldForm.css'





const initialState = {
    total: '',
    date_start: '',
    date_end: '',
}


export default function SoldForm({ onFormSubmit, solde = initialState }) {


    const [form, setForm] = useState(solde);

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return (

        <div className="contains">


            <form onSubmit={handleSubmit} className="soldform"> <br /> <br />
                <h4>Nouveau Solde :</h4>
                <input required type="text" name="total" onChange={handleChange} value={form.total} placeholder="Total" />  <br />
                <input type="date" name="date_start" onChange={handleChange} className="dateinput" value={form.date_start} />
                <input type="date" name="date_end" onChange={handleChange} value={form.date_end} />
                <button className="submit">Modifier</button>

            </form>
        </div>


    )
}