import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import AssignmentIcon from '@material-ui/icons/Assignment';
import './operations.css';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    marginTop: 8,
    
    
  },
}));

export default function Operations({operations, onDelete}) {
  const classes = useStyles();

  return (
    <section className="cardope">
      <List className={classes.root}>
      <ListItem>
     
        <ListItemAvatar>
          <Avatar>
          <AssignmentIcon />
            
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary={operations.title} secondary= {moment(operations.date).fromNow()} />
        <ListItemText primary={operations.total+'€'} />
         <Button className={classes.btn} size="small" color="secondary" onClick={() => onDelete(operations.operaid)}>Delete</Button>
      </ListItem>
    </List>
    </section>
    
  );
}