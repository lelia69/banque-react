import { useState } from "react";
import FormOperations from "./FormOperations";

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import OperationsService from "../shared/operations-service";
import ReactTooltip from "react-tooltip";



export default function Modal() {


    const [open, setOpen] = useState(false)
    const [oper, setOper] = useState([])

    async function addOperations(operations) {
        const response = await new OperationsService().addOpe(operations);

        setOper([
            ...oper,
            response.data
        ]);

    }


    return (
        <>

            <Fab color="primary" aria-label="add" onClick={() => setOpen(!open)} data-tip data-for="btnadd" >
                <AddIcon className="btn-add" data-tip data-for="btnadd" />
            </Fab>

            <ReactTooltip id="btnadd" place="right" effect="solid" border="true" borderColor="rgb(238, 170, 44)" arrowColor="rgb(238, 170, 44)" backgroundColor="rgb(228,67,67)">
                <p>Ajouter une nouvelle Opération</p>
            </ReactTooltip>

            {open && <div>

                <article>
                    <FormOperations onFormSubmit={addOperations} />
                </article>

            </div>}




        </>


    );
}