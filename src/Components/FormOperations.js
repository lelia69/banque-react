import { useState } from "react";
import './FormOperations.css'




const initialState = {
    title: '',
    total: '',
    date: '',
    catid: '',
}


export default function FormOperations({ onFormSubmit, operations = initialState }) {


    const [form, setForm] = useState(operations);

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onFormSubmit(form);
    }

    return (

        <div className="contains">
            

            <form onSubmit={handleSubmit} className="formOpe"> <br/> <br/>


                <input required type="text" name="title" onChange={handleChange} value={form.title} placeholder="Opérations" className="firstinput" />  <br /> <br />

                <input required type="text" name="total" onChange={handleChange} value={form.total} placeholder="Total" /> <br /> <br />

                
                
                <input type="date" name="date" onChange={handleChange} value={form.date} /> <br /> <br />




                <select name="catid" id="" onChange={handleChange}> <br/>  <br/>
                    <option value ="">Selectionner votre categorie</option>
                    <option value="1">Restaurants</option>
                    <option value="2">Maison</option>
                    <option value="3">Vins</option>
                    
                </select>
               
                <button className="btn-form">Envoyer</button>

            </form>
        </div>


    )
}