import axios from "axios";

export default class SoldeService{

     async fetchAllSolde(){
         return axios.get(process.env.REACT_APP_SERVER_URL+"/api/solde/all")
     }

     async addSolde(solde){
         return  axios.post(process.env.REACT_APP_SERVER_URL+"api/solde/add",solde)
     }

     async deleteSolde(id){
         return axios.delete(process.env.REACT_APP_SERVER_URL+"api/solde/delete/"+id)
     }

    /* async updateSolde({item}){
        return axios.patch(process.env.REACT_APP_SERVER_URL+"api/solde/update/"+item.soldeid,{
            total: item.total,
            date_start: item.date_start,
            date_end: item.date_end,
        })
    }*/
}