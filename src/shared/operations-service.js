import axios from "axios";

export default class OperationsService{

   
    async  fetchAllOpe(){
        return await axios.get(process.env.REACT_APP_SERVER_URL+"/api/operations/all")
    }

    async fetchOneOpe(id){
        return await axios.get(process.env.REACT_APP_SERVER_URL+"/api/operations/select/"+id)
    }
    
    async addOpe(operations){
        return await axios.post(process.env.REACT_APP_SERVER_URL+"/api/operations/add", operations)
    }

    async deleteOpe(id){
        return await axios.delete(process.env.REACT_APP_SERVER_URL+"/api/operations/delete/"+id)
    }
}